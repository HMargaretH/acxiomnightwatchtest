"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  fullName: '.sg-type-textbox.sg-question:nth-of-type(1) .sg-input',
  address: '.sg-type-textbox.sg-question:nth-of-type(2) .sg-input',
  city: '.sg-type-textbox.sg-question:nth-of-type(3) .sg-input',
  state: '.sg-input.sg-input-menu option:nth-child(33)',
  zipCode: '.sg-type-textbox.sg-question:nth-of-type(5) .sg-input',
  email: '.sg-type-textbox.sg-question:nth-of-type(6) .sg-input',
  race: 'fieldset:nth-of-type(1) li.sg-last-li label',
  raceInput: 'fieldset:nth-of-type(1) li.sg-last-li input',
  gender: 'fieldset:nth-of-type(2) .sg-first-li label',
  genderInput: 'fieldset:nth-of-type(2) .sg-first-li input',
  isDisabled: 'fieldset:nth-of-type(3) li.sg-last-li label',
  isDisabledInput: 'fieldset:nth-of-type(3) li.sg-last-li input',
  isVeteran: 'fieldset:nth-of-type(4) li.sg-last-li label',
  isVeteranInput: 'fieldset:nth-of-type(4) li.sg-last-li input',
  classification: 'fieldset:nth-of-type(5) .sg-first-li label',
  classificationInput: 'fieldset:nth-of-type(5) .sg-first-li input',
  schoolName: '.sg-type-textbox.sg-question:nth-of-type(7) .sg-input',
  majorSubject: '.sg-type-textbox.sg-question:nth-of-type(8) .sg-input',
  expectedGraduateDate: '.sg-type-textbox.sg-question:nth-of-type(9) .sg-input',
  GPA: '.sg-type-textbox.sg-question:nth-of-type(10) .sg-input',
  consideredPhD: '.sg-type-textbox.sg-question:nth-of-type(11) .sg-input',
  sourceOfKnowledgeAbtProgram: '.sg-question:nth-of-type(12) textarea',
  interestInAcxiom: '.sg-question:nth-of-type(13) textarea',
  summerIntern: 'fieldset:nth-of-type(6) .sg-first-li label',
  summerInternInput: 'fieldset:nth-of-type(6) .sg-first-li input',
  requireSponsorship: 'fieldset:nth-of-type(7) li.sg-last-li label',
  requireSponsorshipInput: 'fieldset:nth-of-type(7) li.sg-last-li input',
  requireSponsorshipInFuture: 'fieldset:nth-of-type(8) li.sg-last-li label',
  requireSponsorshipInFutureInput: 'fieldset:nth-of-type(8) li.sg-last-li input',
  careerVision: '.sg-question:nth-of-type(15) textarea',
  relocateWillingness: 'fieldset:nth-of-type(9) .sg-first-li label',
  relocateWillingnessInput: 'fieldset:nth-of-type(9) .sg-first-li input',
  publicationAgreement: 'fieldset:nth-of-type(10) .sg-first-li label',
  publicationAgreementInput: 'fieldset:nth-of-type(10) .sg-first-li input'
};
exports["default"] = _default;