"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var formXpaths = {
  fullName: '//*[@id="sgE-6532042-3-4-element"]',
  address: '//*[@id="sgE-6532042-3-6-element"]',
  city: '//*[@id="sgE-6532042-3-33-element"]',
  state: '//*[@id="sgE-6532042-3-34-element"]/option[33]',
  zipCode: '//*[@id="sgE-6532042-3-36-element"]',
  email: '//*[@id="sgE-6532042-3-46-element"]',
  race: '//*[@id="sgE-6532042-3-48-box"]/div/ul/li[6]/label',
  gender: '//*[@id="sgE-6532042-3-21-box"]/div/ul/li[2]/label',
  isDisabled: '//*[@id="sgE-6532042-3-14-box"]/div/ul/li[2]/label',
  isVeteran: '//*[@id="sgE-6532042-3-15-box"]/div/ul/li[5]/label',
  classification: '//*[@id="sgE-6532042-3-16-box"]/div/ul/li[1]/label',
  schoolName: '//*[@id="sgE-6532042-3-17-element"]',
  majorSubject: '//*[@id="sgE-6532042-3-18-element"]',
  expectedGraduateDate: '//*[@id="sgE-6532042-3-19-element"]',
  GPA: '//*[@id="sgE-6532042-3-20-element"]',
  consideredPhD: '//*[@id="sgE-6532042-3-26-element"]',
  sourceOfKnowledgeAbtProgram: '//*[@id="sgE-6532042-3-27-element"]',
  interestInAcxiom: '//*[@id="sgE-6532042-3-28-element"]',
  summerIntern: '//*[@id="sgE-6532042-3-54-box"]/div/ul/li[1]/label',
  requireSponsorship: '//*[@id="sgE-6532042-3-53-box"]/div/ul/li[2]/label',
  requireSponsorshipInFuture: '//*[@id="sgE-6532042-3-51-box"]/div/ul/li[2]/label',
  careerVision: '//*[@id="sgE-6532042-3-29-element"]',
  relocateWillingness: '//*[@id="sgE-6532042-3-30-box"]/div/ul/li[1]/label',
  publicationAgreement: '//*[@id="sgE-6532042-3-32-box"]/div/ul/li[1]/label'
};
var _default = formXpaths;
exports["default"] = _default;