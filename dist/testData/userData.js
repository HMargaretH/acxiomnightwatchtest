"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  fullName: 'Bridget Johnson',
  address: '41th Avenue',
  city: 'New York',
  zipCode: '10005',
  email: 'xyz@gmail.com',
  schoolName: 'Cambridge University',
  majorSubject: 'Computer Science',
  expectedGraduateDate: '06/25/22',
  GPA: '3.9',
  consideredPhD: 'Yes, PhD, Cambridge University - after graduation',
  sourceOfKnowledgeAbtProgram: 'I have read leaflet pinned to bulletin board in a campus and decided to try',
  interestInAcxiom: 'The approach of putting the customers and their needs in the first place',
  careerVision: 'Yes, I would like to work with Data Bases and support IT Team with Back-End Development'
};
exports["default"] = _default;