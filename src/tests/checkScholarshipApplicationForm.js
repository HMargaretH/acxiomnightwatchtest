import userData from '../testData/userData';
import formCssSelectors from '../testData/formCssSelectors';

module.exports = {
    'Navigate to form': browser => {
        const expandMenu = '.hamburger .hamburger-box';
        const menuItemCareer = '.js-nav .menu > .menu-item:nth-of-type(8) span';
        const scholarshipApplication = '.inline-list li:nth-of-type(2) a';
        browser
            .url('https://www.acxiom.com/')
            .click(expandMenu)
            .click(menuItemCareer)
            .click(scholarshipApplication)
            .end();
    },

    'Fill the form and Check If Saved' : browser => {
        const surveyNextButton = '.sg-button-bar input';
        const surveyBackButton = '.sg-button-bar input:nth-child(2)';
        const surveyCompleteNextButton = '.sg-button-bar input:nth-child(1)';

        browser
            .url('https://survey.alchemer.com/s3/6532042/2022-Acxiom-Diversity-Scholarship-Application')
            .click(surveyNextButton)
            .setValue(formCssSelectors.fullName, userData.fullName)
            .setValue(formCssSelectors.address, userData.address)
            .setValue(formCssSelectors.city, userData.city)
            .click(formCssSelectors.state)
            .setValue(formCssSelectors.zipCode, userData.zipCode)
            .setValue(formCssSelectors.email, userData.email)
            .waitForElementVisible(formCssSelectors.race, 3000)
            .click(formCssSelectors.race)
            .click(formCssSelectors.gender)
            .click(formCssSelectors.isDisabled)
            .click(formCssSelectors.isVeteran)
            .click(formCssSelectors.classification)
            .setValue(formCssSelectors.schoolName, userData.schoolName)
            .setValue(formCssSelectors.majorSubject, userData.majorSubject)
            .setValue(formCssSelectors.expectedGraduateDate, userData.expectedGraduateDate)
            .setValue(formCssSelectors.GPA, userData.GPA)
            .setValue(formCssSelectors.consideredPhD, userData.consideredPhD)
            .setValue(formCssSelectors.sourceOfKnowledgeAbtProgram, userData.sourceOfKnowledgeAbtProgram)
            .setValue(formCssSelectors.interestInAcxiom, userData.interestInAcxiom)
            .waitForElementVisible(formCssSelectors.summerIntern, 3000)
            .click(formCssSelectors.summerIntern)
            .click(formCssSelectors.requireSponsorship)
            .click(formCssSelectors.requireSponsorshipInFuture)
            .setValue(formCssSelectors.careerVision, userData.careerVision)
            .click(formCssSelectors.relocateWillingness)
            .click(formCssSelectors.publicationAgreement)
            .click(surveyBackButton)
            .click(surveyNextButton)
            .assert.valueContains(formCssSelectors.fullName, userData.fullName)
            .assert.valueContains(formCssSelectors.address, userData.address)
            .assert.valueContains(formCssSelectors.city, userData.city)
            .assert.selected(formCssSelectors.state)
            .assert.valueContains(formCssSelectors.zipCode, userData.zipCode)
            .assert.valueContains(formCssSelectors.email, userData.email)
            .waitForElementVisible(formCssSelectors.race, 3000)
            .verify.attributeEquals( formCssSelectors.raceInput, 'checked', 'true')
            .verify.attributeEquals(formCssSelectors.genderInput, 'checked', 'true')
            .verify.attributeEquals(formCssSelectors.isDisabledInput, 'checked', 'true')
            .verify.attributeEquals(formCssSelectors.isVeteranInput, 'checked', 'true')
            .verify.attributeEquals(formCssSelectors.classificationInput, 'checked', 'true')
            .assert.valueContains(formCssSelectors.schoolName, userData.schoolName)
            .assert.valueContains(formCssSelectors.majorSubject, userData.majorSubject)
            .assert.valueContains(formCssSelectors.expectedGraduateDate, userData.expectedGraduateDate)
            .assert.valueContains(formCssSelectors.GPA, userData.GPA)
            .assert.valueContains(formCssSelectors.consideredPhD, userData.consideredPhD)
            .assert.valueContains(formCssSelectors.sourceOfKnowledgeAbtProgram, userData.sourceOfKnowledgeAbtProgram)
            .assert.valueContains(formCssSelectors.interestInAcxiom, userData.interestInAcxiom)
            .waitForElementVisible(formCssSelectors.summerIntern, 3000)
            .verify.attributeEquals(formCssSelectors.summerInternInput, 'checked', 'true')
            .verify.attributeEquals(formCssSelectors.requireSponsorshipInput, 'checked', 'true')
            .verify.attributeEquals(formCssSelectors.requireSponsorshipInFutureInput, 'checked', 'true')
            .assert.valueContains(formCssSelectors.careerVision, userData.careerVision)
            .verify.attributeEquals(formCssSelectors.relocateWillingnessInput, 'checked', 'true')
            .verify.attributeEquals(formCssSelectors.publicationAgreementInput, 'checked', 'true')
            .click(surveyCompleteNextButton)
            .end();
    }
}