# AcxiomNightwatchTest

## What is it 

This is a small project I am learning on the Nightwatch Framework with use of Acxiom site. 

## How to launch 

1. Install node on your pc (link to download: https://nodejs.org/en/)
2. Run `npm install` on this project (from cmd)
3. Run `npm run test` in order to run tests
